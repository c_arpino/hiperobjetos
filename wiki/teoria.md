## O que é um CNC e como funciona

A sigla [CNC](https://pt.wikipedia.org/wiki/Comando_num%C3%A9rico_computadorizado) significa Computer Numeric Control, controle numérico computadorizado. Uma máquina CNC é uma máquina automatizada que é controlada por um computador através de comandos numéricos. A exemplo, vamos analisar tornos: um [torno mecânico](https://pt.wikipedia.org/wiki/Torno_mec%C3%A2nico) normal é manuseado por um técnico que faz todas as movimentações da máquina controlando-a por completo. Já em um torno CNC, o técnico, auxiliado de softwares próprios para isso, cria um arquivo [G-Code](https://en.wikipedia.org/wiki/G-code), que é um arquivo com comandos para a máquina, que contém coordenadas que são interpretadas pela placa controladora do CNC que são traduzidas para as configurações da máquina e então a máquina executa todas as movimentações e comandos de forma autonoma.

### Motores de passo

Os [motores de passo](https://pt.wikipedia.org/wiki/Motor_de_passo) são motores que possuem varios imãs permanentes no seu interior que atuam em conjunto com eletroímãs. A combinacao de muitos imãs com eletroímãs permite ao motor de passo ficar estático em diferentes posições , dependendo de como é controlada a corrente e tensão dos eletroímãs é possível controlar os campos magnéticos. Estando o motor em uma posicao, basta variar um pouco esse conjunto que o eixo do motor vai se mover um pouco variando sua posição angular, isso representa um passo. Um motor de passo comumente pode ter ate 200 posições  diferentes em 360 graus, ou seja, 200 passos em uma volta completa. A cada passo dado o eixo do motor gira 1.8 graus. Dessa maneira é possível controlar quantos graus o motor se move em sentido horário ou anti-horário.

### Funcionamento do CNC

Todo CNC funciona com motores de passo e existem diversas formas de os motores de passo atuarem em um CNC, como uma [impressora 3D](https://pt.wikipedia.org/wiki/Impress%C3%A3o_3D) tradicional que se move em um sistema cartesiano de 3 dimensoes e uma impressora modelo Delta que se move atráves de 3 eixos verticais. Para entender como funciona um CNC vamos pensar em um CNC que se mova em um sistema cartesiano. Neste caso, os motores de passo que fazem a máquina se mover, normalmente são 3 e estão distribuidos nos 3 eixos da máquina, os eixos X, Y e Z. Existem duas maneiras comuns de transformar o movimento circular dos motores em movimentos lineares para a máquina se mover no eixo, por correias e polías ou por  fusos e castanhas. A exemplo, o de correias e polías basicamente, em uma extremidade do eixo de deslocamento tem-se um motor com uma polia acoplada a ele e uma outra polía na outra extremidade, as polías são ligadas por uma correia,  ao girar do motor de passo a correia percorre todo eixo de deslocamento, transformando o movimento angular do motor em movimento linear. Ao prender o carro(ferramenta que a máquina utiliza como laser ou spindle dentre outros) obtemos que a ferramenta se move naquele eixo, fazendo isso para os outros eixos temos que a ferramenta pode se mover em todas as 3 dimensoes. E como tudo isso acontece pelo movimento do motor de passo e que temos o controle de quanto o motor varia sua posicao angular, consequentemente, temos o controle de quanto o carro se move linearmente tendo uma relação direta entre graus variados pelo eixo do motor e milímetros andados pelo carro.

### G-Code

Os CNCs são controlados por um codigo chamado [G-Code](https://en.wikipedia.org/wiki/G-code), que é um arquivo com informações das coordenadas cartesianas que o carro deve percorrer para executar o trabalho solicitado pelo técnico operador da máquina. Essas coordenadas são interpretadas pelo firmware da placa de controle do CNC.

### Firmware

O papel do firmware, como o GRBL(firmware opensource para CNC), é de traduzir as coordenadas do arquivo g-code em números de passo que o motor deve dar. E para isso ele tem configurações que são mudadas de máquina para máquina. Dependendo de como é o CNC ele pode ter comandos g-code diferentes de outros. Como um CNC de corte a laser vai ter comandos diferentes de um router CNC.










