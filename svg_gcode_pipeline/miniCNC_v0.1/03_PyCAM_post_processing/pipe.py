#!/usr/bin/python3

# Rellenar Gcodes faltantes (por reciclado)
# Uso:
#		ir al directorio con el .gcode/.nc output de PyCAM
# 		$ python3 pipe.py  # usar esto si python3 vive en otro directorio
#		$ ./pipe.py

from sys import argv
import re
from copy import deepcopy as dcopy

if len(argv) != 3:
	raise ValueError("No hay suficientes argumentos")

f = open(argv[1], "r")
o = open(argv[2], "w") 

g_prev = "F300"
d = {}

def fill_Gcode_00(line, g_prev = "F300"):
	l = re.split(" |\t", line)  # Partir la línea e espacios o tabs
	g = l[0]
	# print(l)

	if g != "":
		# Si el inicio de la línea no es vacío, debe ser un gcode.
		g_prev = g  # Save the value
		return([line, g_prev])  # Write the value

	elif g == "":
		# Si el inicio de la línea es vacío, hay que reciclar el gcode anterior.
		return(["".join([g_prev, line]), g_prev])  # Write the value


def replace_Z_01(line):
	# Busscar algo como G1 Z0.0000
	m = re.search("^G[01]\\s*Z([\\.\\d]*.*)", line)

	if m is not None:
		# Si encontramos algo, debe ser un cambio en Z

		# z = float(m.group(1))
		# round

		# Obtener el valor
		z = m.group(1)


		if re.search("[123456789]", z) is None:
			# Si contiene algun dígito no cero, debe ser abajo "Z0.00000"
			return("M300 S30 (pen down)\nG4 P50 (wait 50ms)\n")
		else:
			# Si no, debe ser arriba
			return("M300 S50 (pen up)\nG4 P50 (wait 50ms)\n")
	else:
		# Si no encontramos nada, imprimir la linea tal cual.
		return(line)


def replace_g0_02(line):
	# Replace "G0 " with "G1 " if found
	l = re.sub(
		"G0 ",
		"G1 ",
		line
		)

	return(l)


def fill_XYZ_03(line, d = {}, dims = "XY", r = re.compile('^G1 .*') ):
	m = r.match(line)

	if m == None:
		# If the line is not a G1 instruction, return line and dictionary unmodified.
		return([line, d])

	else:
		# If it is a G1 instruction, loop over its coordinates until a full "G1 X Y Z" is built.
		for index, axis in enumerate(dims):
			r2 = re.compile('(%s[\\d\\.]+)' % axis	)
			m2 = r2.search(line)

			if m2 is not None:
				# If the dimention is found, save it in the dictionary
				d[axis] = m2.group()

		if None not in d.values():
			# If the dictionary has no null values, all coordinates have been assigned.
			# Return the line
			return(["G1  " + "  ".join(d.values()) + "\n", d])
		else:
			# If not, then return the line anyway... filled with zeros
			# If the line is incomplete, miniCNC v0.1 firmware will ignore it,
			# and that is undesirable.

			d2 = dcopy(d)  # Make an unreferenced copy of the dictionary

			# Fill "d2" with 0.0 if undefined
			for index, axis in enumerate(dims):
				if d2[axis] is None:
					d2[axis] = axis + "0.0"
				else:
					continue

			# Return the filled line, but return the original "d" instead of modified "d2"
			return(["G1  " + "  ".join(d2.values()) + "\n", d])

# Write header
o.write("G21 (metric ftw)\nG90 (absolute mode)\nG92 X0.00 Y0.00 Z0.00 (you are here)\n\nM300 S30 (pen down)\nG4 P50 (wait 50ms)\nM300 S50 (pen up)\nG4 P50 (wait 50ms)\nM18 (disengage drives)\nM01 (Was registration test successful?)\nM17 (engage drives if YES, and continue)\n")
o.write("\n(Header end)\n\n")

# Write gcode body
f_lines = f.readlines()
for line in f_lines:
	# Skip anything starting with F, S, M, "\n", G02-G09 or G2-G9
	# These commands are not recognized by miniCNC firmware v0.1
	r = re.compile("^(F|S|M|\n|G0{0,1}[2-9])")
	l = r.match(line)

	if l is not None:
		o.write("\n")  # May be unnecesary
		continue
	else:
		# Else the line is relevant, so handle and save it
		line, g_prev = fill_Gcode_00(line, g_prev)

		line = replace_Z_01(line)

		line = replace_g0_02(line)

		line, d = fill_XYZ_03(line, d = d, dims = "XY", r = re.compile('^G1.*') )

		o.write(line)

# Write footer
o.write("\n(Footer begin)\n")
o.write("(end of print job)\nM300 S50.00 (pen up)\nG4 P50 (wait 50ms)\nM300 S255 (turn off servo)\nG1 X0 Y0 F3000.00\nG1 Z0.00 F150.00 (go up to finished level)\nG1 X0.00 Y0.00 F3000.00 (go home)\nM18 (drives off)\n")

# Close all
f.close()
o.close()
