 
# Firmware y Software

Un video demostrando el uso se encuentra disponible en [Vimeo](https://vimeo.com/352326473).

## Firmware
Usamos el sketch del tutorial para el miniCNC 0.1. El código está disponible en el [repositorio](https://tecnologias.libres.cc/hiperobjetos/minicnc/tree/master/arduino/miniCNC_v0.1/). Es importante saber que el firmware no es estándar, solo reconoce cierto tipo de Gcode muy limitado.

Queremos usar [GRBL](https://github.com/grbl/grbl/wiki/Flashing-Grbl-to-an-Arduino) para el miniCNC 0.2

### Calibración
Modificamos los límites del movimiento del servo para hacerlo más exagerado durante las primeras pruebas estructurales.

Para la calibración, enviamos códigos `U` (lapircera arriba) y `D` (lapiecra abajo) desde la consola serial del Arduino IDE. Luego observamos si el desplazamiento era adecuado, y repetimos el proceso hasta que quedó bien.

		// Pruebas (movimiento exagerado)
		const int penZUp = 0;
		const int penZDown = 166;
		
		// Final (valores calibrados)
		const int penZUp = 130;
		const int penZDown = 90;

Para probar los límites en ZY, enviamos códigos `G1 X0 Y0` (origen) y `G1 X40 Y40` (extremo) desde la serial console de Arduino IDE.

Usamos el Gcode del tutorial "Hisoka.gcode" para hacer las pruebas generales. También hay dos Gcode para probar movimientos en XY (steppers) o para Z (servo) disponibles en el [repositorio](https://tecnologias.libres.cc/hiperobjetos/minicnc/tree/master/svg_gcode_pipeline/miniCNC_v0.1/).

  * _testPen.gcode_ tiene dos instrucciones `U` y `D` que son reconocidas por el firmware miniCNC 0.1 para subir y bajar el servo de la lapicera.
  * _testXY.gcode_ debería dibujar un cuadrado.

### Pruebas
Para hacer las pruebas modificamos el firmware de [este tutorial](https://www.youtube.com/watch?v=rv7WUX7gFkw). El archivo _sketch_apr01a_testing.ino_ está disponible en el [repositorio](https://tecnologias.libres.cc/hiperobjetos/minicnc/tree/master/arduino/miniCNC_v0.1/).



El firmware está modificado para reconocer un gcode "G4" y hacer una demora de 1 segundo `delay(1000)`. Lo usamos solamente en _testPen.gcode_ y _testXY.gcode_.

Para las pruebas, usamos el archivo "Hisoka.gcode" del tutorial.

## Software
Queremos usar Inkscape, FreeCAD u otros para generar los dibujos.

Deberíamos encontrar un buen generador de Gcode, o un firmware que interprete correctamente el output de Inkscape + Unicorn, u otros.

Pronterface funciona _bien_ para conectarse y enviar instrucciones.

### Opción 1: Inkscape + Unicorn + Pronterface
No recomendada.

Esta es la que usan en el tutorial. Sin embargo, la extensión está desactualizada y tiene algunos problemas:
  * El origen de las coordenadas está desplazado.
  * No es posible configurar las unidades del documento en "mm", porque falla la exportación a .gcode usando "Save As".

### Opción 2: Inkscape + [PyCAM](http://pycam.sourceforge.net/) + [_script_](https://tecnologias.libres.cc/hiperobjetos/minicnc/blob/master/svg_gcode_pipeline/miniCNC_v0.1/03_PyCAM_post_processing/pipe.py) + Pronterface

Un video demostrando el uso se encuentra disponible en [Vimeo](https://vimeo.com/352326473).

Resolvimos usar Inkscape solamente para generar el SVG.

#### 1 - Primer Paso - Inkscape

1. Hacer dibujo
2. Vaciar el "fill"
3. Convertir los objetos a "path" y combinarlos.
4. Guardar. Recomendamos generar 3 carpetas: 1 de SVG. Otra del archivo gcode generado por PyCAM (ver paso 2 ) y luego otra de gcode procesado (para guardar el archivo modificado).

#### 2 - Segundo Paso - PyCAM

Usamos PyCAM para generar el Gcode

0. [Requisitos](http://pycam.sourceforge.net/requirements/) para instalar.
Además de los requisitos allí enumerados, fue necesario instalar svg_path usando pip, para python2.
1. Usar normalmente, no es necesaria ninguna modificación.
  * Aparecerá el modelo ejemplo de PyCAM, borrar. Cargar el SVG como modelo. Luego se deberán crear 1 Tool, 1 Processe, 1 Bound, 1 Task.
  * Para la creación de Processes, elegir en el dropdown desplegable Engraving para el ejemplo de este tutorial. El modelo debe figurar seleccionado antes de continuar a los siguientes pasos.
  * Para el Bound seleccionar Type Custom y en ejes X e Y colocar 0 en lower y luego 40 en upper los campos correspondientes
  * Para la Task se crea una nueva y esta incluirá los pasos anteriores creados. Veremos que se activa el botón Toolpath
  * Luego se exporta el toolpath con el botón Export All.
2. Después de exportar, cambiar la extensión del output a ".gcode"

#### 3 - Post-procesamiento

Ejecutar el script "pipe.py" sobre ese archivo, para producir un Gcode compatible con el firmware del miniCNC v0.1.

`$ ./pipe.py input.gcode output.gcode`

#### 4 - Usar Pronterface

0. Ajustar el tamaño del lienzo en Settings, Options, Build Dimensions. 
1. Cargar el Gcode
2. Conectar al Arduino
3. Print!

