# Cómo armar la electrónica

## Materiales
  * Alicate
  * Cable IDE
  * Soldadora
  * Estaño
  * Pelacables

## 00 - Preparar cable de cinta

Más adelante soldaremos cables cada motor. Vamos a reciclar los cables IDE que vienen con la unidad, pero es posible usar cualquier cable para soldar.

  1. Cortar los extremos de la cinta de cables, para remover los conectores de plástico.
  2. Con el alicate, hacer un corte longitudinal que separe la punta de un grupo de 4 cables del resto del manojo.
  3.  Para separarlos completamente del manojo, sostener el manojo con las manos y tirar de los 4 cables.
  4. Con una trincheta o alicate, hacer cortes longitudinales para separar solamente las puntas de los 4 cables (NO separarlos completamente).
  5. Pelar las 4 puntas con un alicate.
  6. Repetir estos pasos para el otro extremo de los 4 cables.

### [Video cable IDE](https://vimeo.com/352120131)

## 01 - Contactos en en los motores paso a paso

Los motores paso a paso del DVD tienen dos bobinas, y dos contactos por cada una.

  1. Usar un multímetro (en modo continuidad) para saber cuales contactos están conectados, y anotar a qué bobina corresponden (A o B).
  2. Soldar cables a cada contacto.
  3. Es importante la polaridad de cada bobina, pero para conocerlas haremos más adelante una prueba, directamente con el Arduino + Motor Control Shield.

### [Video soldadura](https://vimeo.com/352123854)

## 02 - Arduino Uno

La fuente de poder del arudino debe ser [adecuada](https://www.open-electronics.org/the-power-of-arduino-this-unknown).

Inicialmente usamos una fuente DC 12V = 2A conectada al JAPAN JACK del Arduino, de forma que alimenta al shield también.

Siguiendo el tutorial y otras recomendaciones, decidimos cambiar y usar fuentes independientes: una de 6V-9V conectada solamente al shield, y alimentamos el Arduino por USB. Para esto colocamos el _VIN jumper_ en los pines.

Nosotros pensábamos que como el _Vin jumper_ estaba conectado, el Adafruit Motor Shield (v1) proveería energía al servo. Sin embargo, este siempre se alimenta del Arduino, y eso nos generó problemas:
  * No logramos controlar el servo, este se movía de forma impredecible.
  * La conexión con el Arduino era inestable y se reiniciaba.

![Vin Jumper](2-conexiones-vin_jumper.png)

Finalmente resolvimos quitar el _Vin jumper_ (como se muestra en la figura anterior) y volver a alimentar todo el sistema a través del JAPAN JACK del Arduino. Usamos una fuente regulada para estimar los requisitos de energía de todo el sistema.

Requisitos mínimos de energía para nuestro miniCNC:
  * 9 V
  * 0.7-0.8 A

A menor voltaje, los motores perdían pasos. Si el miniCNC no parecía funcionar bien, con frecuencia se debió a falta de voltaje (algunas veces tuvimos que usar 12V).

## 03 - Conexiones
No estaba disponible el modelo de Fritzing para la versión 1 del motor shield, así que usamos el modelo del [motor shield v2](https://github.com/adafruit/Fritzing-Library/tree/master/parts) de Adafruit ([archivo fzpz](2-Adafruit-Motor-Stepper-Servo-Shield.fzpz) del shield).

Son diferentes placas, pero las conexiones son equivalentes ([archivo fzz](2-conexiones.fzz) del esquema.).

![Conexiones entre el Arduino y los motores.](2-conexiones.png)

Es importante que el polo positivo del shield quede conectado al "tip" interno del _Jack_. Esta es la configuración estándar, ya que la mayoría de las fuentes tienen el polo positivo en el centro, y el negativo en la vaina del conector.
